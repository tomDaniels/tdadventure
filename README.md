Overview
====

This is a basic text-adventure game created as a learning exercise at the
"from south bend import python" meetup group.

Ideas
====

This is a list of things we discussed as a group that we should eventually
incorporate into the program.

* Rooms
* Objects
* Directions
* Points
* Since we are using Python3 maybe go over how to type annotations and add them
* Add a "help" verb to the base adventure class which will tell the user about
  the current game they are in (its name), some basic verbs (such as those
  implemented by the base class), and anything else important.
* If there are multiple adventures to pick from and the user types a value that
  is not a number an exception is thrown. This should be handled better.
* Add a "look" verb to the base class.
* Allow each adventure to have an "intro" text that is shown at the start of
  the adventure to help set the stage and explain the goal the player is working
  towards.
* Save games and restore later?
* Scoreboard of top scores in each game
* Create a proper setup.py so that users can just "pip install adventure"
* Does the base class need a way to handle "winning" or "finishing" the game?
  Perhaps some method that is called or a state variable that is part of the
  class. In the case of a variable we can check it inside the start_adventure
  method after each loop to see if the game has finished (or perhaps they player
  lost)
* Format long room descriptions to a maximum width of something like 50
  characters so that it looks better in the terminal. There is a built in
  python module for this.
* "accomplishments" in an adventure.
